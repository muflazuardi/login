package muflazuardi.layoutlogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText username, password;
        Button btnLogin;

        username = (EditText)findViewById(R.id.editUser);
        password = (EditText)findViewById(R.id.editPass);
        btnLogin = (Button)findViewById(R.id.button);
        btnLogin.setOnClickListener(this);
    }
    public void onClick(View v){
        Toast.makeText(login.this,"Button Clicked", Toast.LENGTH_SHORT).show();
    }
}
